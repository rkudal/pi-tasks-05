#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
struct BOOK
{
	char title[256];
	char fio[256];
	int year; 
};
struct BOOK *data;
int maxIndex = -1, minIndex = -1;
void readBook (FILE *fp, struct BOOK *st, int Index)
{
	char buf[256];
	static int max=0;
	static int min=2016;
	fgets(st->title, 256, fp);
	fgets(st->fio, 256, fp);
	fgets(buf, 256, fp);
	st->year = atoi(buf);

	// At the same time we're searching for the oldest and the latest book 

	if (st->year>max)
	{	
		maxIndex=Index;
		max=st->year;
	}
	if (st->year<min)
	{	
		minIndex=Index;		
		min=st->year;
	}
}
void printBook(FILE *fp, struct BOOK *st)
{
	printf("%s%s%d\n\n", st->title, st->fio, st->year);
}
void StructArrSort(int size)
{
	int i, j, temp_year;
	char temp_fio[256] = {'\0'};
	char temp_title[256] = {'\0'};
	for (i = 1; i < size; i++)
	{
		j = i;
		strcpy(temp_fio, data[i].fio);
		strcpy(temp_title, data[i].title);
		temp_year = data[i].year;
		while (j > 0 && strcmp(temp_fio, data[j-1].fio)<0)
		{
			strcpy(data[j].fio, data[j-1].fio);
			strcpy(data[j].title, data[j-1].title);
			data[j].year = data[j-1].year;
			j--;
		}
		strcpy(data[j].fio, temp_fio);
		strcpy(data[j].title, temp_title);
		data[j].year=temp_year;
	}
}
int main()
{
	FILE *fp;
	int i=0, pos, end;
	int memory = 0;
	fp = fopen("books.txt", "rt");
	if (fp == NULL)
	{
		perror("File books.txt:");
		return 1;
	}
	fseek(fp, 0, SEEK_END);		
	end = ftell(fp);
	rewind(fp);	
	pos = ftell(fp);

	// Allocating memory for struct array 

	while (pos!=end)
	{
		data = (struct BOOK *)realloc(data, (memory += 1)*sizeof(struct BOOK));
		readBook (fp, data + i, i);
		printBook(fp, data + i);
		i++;
		pos = ftell(fp);
	}

	// Informs user if there is no data in the file 

	if (maxIndex == -1 || minIndex == -1)
	{	puts("No data in the file");
		return 1;
	}
	else
	{
		printf("\nThe oldest book:\n%s%s%d\n", data[minIndex].title, data[minIndex].fio, data[minIndex].year);
		printf("\nThe latest book:\n%s%s%d\n", data[maxIndex].title, data[maxIndex].fio, data[maxIndex].year);
	}
	StructArrSort(memory);

	// Printing sorted book list

	puts("\n\nSorted book list:\n");
	for (i=0; i<memory; i++)
		printBook(fp, data + i);
	free(data);
	return 0;
}